
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class WFReporter{

	Document dom;
	List<Step> steps;
	static final Hashtable<String, String> humanReadableTable = new Hashtable<String, String>(){{
		put("com.atlassian.jira.workflow.condition.AllowOnlyAssignee","Only Assignee can execute");
		put("com.onresolve.jira.groovy.GroovyFunctionPlugin","Run Script as Post Function");
		put("com.innovalog.jmwe.plugins.functions.IncreaseFieldValueFunction","Increase Value of the Field");
		put("com.atlassian.jira.workflow.function.event.FireIssueEventFunction","Trigger Event");
		put("com.atlassian.jira.workflow.condition.InProjectRoleCondition","User must be in Project Role");
		put("com.onresolve.jira.groovy.GroovyValidator","Run Script as Validator");
		put("com.atlassian.jira.workflow.function.issue.AssignToReporterFunction","Assign Issue to Reporter");
		put("com.innovalog.jmwe.plugins.functions.CopyValueFromOtherFieldPostFunction","Copy Value from Field to other Field");
		put("com.innovalog.jmwe.plugins.conditions.NonInteractiveCondition","Hide this transition");
		put("com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction","Update issue field");
		put("com.innovalog.jmwe.plugins.conditions.PreviousStatusCondition","Previous Status must be");
		put("com.atlassian.jira.workflow.condition.AllowOnlyReporter","Only Reporter can execute");
		put("com.atlassian.jira.workflow.validator.PermissionValidator","User must have permission");
		put("com.onresolve.jira.groovy.canned.workflow.conditions.AllSubtasksResolvedCondition","All Subtasks must be resolved");
		put("com.googlecode.jsu.workflow.validator.FieldsRequiredValidator","Following Fields are required");
		put("com.onresolve.jira.groovy.GroovyCondition","Run script as Condition");
		put("com.atlassian.jira.workflow.condition.UserInGroupCondition","User must be in group");
		put("com.innovalog.jmwe.plugins.functions.AssignToLastRoleMemberFunction","Assign to Last Role Member");
		put("field.name","Field Name: ");
		put("sourceField","Source Field: ");
		put("includeCurrentAssignee","Include Current Assignee: ");
		put("includeReporter","Include current Reporter: ");
		put("destinationField","Destination Field: ");
		put("com.atlassian.jira.workflow.function.issue.AssignToCurrentUserFunction","Assign to Current User");
		put("com.innovalog.jmwe.plugins.validators.CommentRequiredValidator","Comment required");
//		put("includeCurrentAssigneeno","Doesn't include Current Assignee");
//		put("includeReporterno","Doesn't include current Reporter");
		

	}};

	public WFReporter(){
		steps = new ArrayList<Step>();
	}

	private void parseXmlFile(String fileName){
		//get the factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {

			//Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			dom = db.parse(fileName);


		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private void parseDocument(){
		//get the root element
		NodeList stepsNL = dom.getDocumentElement().getElementsByTagName("steps");

		//get the first steps element(there is only one)
		Element stepsElement = (Element)stepsNL.item(0);

		NodeList step = stepsElement.getElementsByTagName("step");
		if(step != null && step.getLength() > 0) {
			for(int n = 0 ; n < step.getLength();n++) {

				Element elm = (Element)step.item(n);
				Step e = getStep(elm);

				NodeList action = elm.getElementsByTagName("action");
				if(action != null && action.getLength() > 0) {
					for(int i = 0 ; i < action.getLength();i++) {

						Element elem = (Element)action.item(i);
						Action a = getAction(elem);
						e.getActions().add(a);
						getValidators(elem,a);
						getConditions(elem,a);
						getPFunctions(elem,a);
					}
				}
				//add it to step list
				steps.add(e);
			}
		}
	}

	private Step getStep(Element empEl) {

		//for each <Step> element get text or int values of 
		String name = empEl.getAttribute("name");
		int id = Integer.parseInt(empEl.getAttribute("id"));
		//Create a new Step with the value read from the xml nodes
		Step e = new Step(id,name);

		return e;
	}

	private void getValidators(Element empEl,Action act) {

		NodeList validator = empEl.getElementsByTagName("validator");
		if(validator != null && validator.getLength() > 0) {
			for(int n = 0 ; n < validator.getLength();n++) {

				Element elm = (Element)validator.item(n);
				NodeList action = elm.getElementsByTagName("arg");
				if(action != null && action.getLength() > 0) {
					for(int i = 0 ; i < action.getLength();i++) {

						Element elem = (Element)action.item(i);

						String validatorClassname = getRidOfClassName(elem.getAttribute("name"));

						act.validators.add(validatorClassname.concat(" ").concat(makeItReadable(elem.getTextContent())));
					}
				}				
			}

		}
	}

	private void getConditions(Element empEl,Action act) {

		NodeList condition = empEl.getElementsByTagName("condition");
		if(condition != null && condition.getLength() > 0) {
			for(int n = 0 ; n < condition.getLength();n++) {

				Element elm = (Element)condition.item(n);
				NodeList arg = elm.getElementsByTagName("arg");
				if(arg != null && arg.getLength() > 0) {
					for(int i = 0 ; i < arg.getLength();i++) {

						Element elem = (Element)arg.item(i);
						String conditionClassname = getRidOfClassName(elem.getAttribute("name"));

						act.conditions.add(conditionClassname.concat(" ").concat(makeItReadable(elem.getTextContent())));
					}
				}				
			}
		}
	}

	private void getPFunctions(Element empEl,Action act) {

		NodeList function = empEl.getElementsByTagName("function");
		if(function != null && function.getLength() > 0) {
			for(int n = 0 ; n < function.getLength();n++) {

				Element elm = (Element)function.item(n);
				NodeList arg = elm.getElementsByTagName("arg");
				if(arg != null && arg.getLength() > 0) {
					for(int i = 0 ; i < arg.getLength();i++) {

						Element elem = (Element)arg.item(i);
						if(act.isPostFunctionWorthy(elem.getTextContent())){
							String postFunction = getRidOfClassName(elem.getAttribute("name"));
							act.postFunctions.add(postFunction.concat(makeItReadable(elem.getTextContent())));
						}
					}	
				}				
			}
		}
	}
	private Action getAction(Element empEl) {

		//for each <Action> element get text or int values of 
		String name = empEl.getAttribute("name");
		int id = Integer.parseInt(empEl.getAttribute("id"));
		int targetID = 0;
		NodeList resultNL = empEl.getElementsByTagName("results");
		if(resultNL != null && resultNL.getLength() > 0){
			Element resultElement = (Element)resultNL.item(0);
			NodeList action = resultElement.getElementsByTagName("unconditional-result");

			targetID=Integer.parseInt(((Element)action.item(0)).getAttribute("step"));
		}		
		//Create a new Action with the value read from the xml nodes
		Action a = new Action(id,name,targetID);

		return a;
	}

	public String findStepNameByID(int ID){
		Step s = new Step();

		for(int i=0;i<this.steps.size();i++)
			if(this.steps.get(i).getId()==ID){
				s=steps.get(i);
				break;
			}

		return s.getName();
	}


	public static String makeItReadable(String jiraLanguage){

		return humanReadableTable.get(jiraLanguage)!=null ?  humanReadableTable.get(jiraLanguage) : jiraLanguage;

	}

	public static String getRidOfClassName(String jiraLanguage){

		return jiraLanguage.equals("class.name") ?  "" : jiraLanguage;

	}

	public static boolean isXML(String fileName){

		return fileName.endsWith("xml") ? true : false;

	}

	public static void main(String[] args){
		WFReporter wf = new WFReporter();


		String fileName = "";

		if(args.length<1){
			System.out.println("Please specify an XML file");
			System.exit(0);
		}
		fileName=fileName.concat(args[0]);
		if(!isXML(fileName)){
			System.out.println("Please specify an XML file");
			System.exit(0);
		}
		wf.parseXmlFile(fileName);
		wf.parseDocument();
		
		
		FileWriter fstream;
		try {
			fstream = new FileWriter(fileName.replace("xml", "csv"));
			BufferedWriter out = new BufferedWriter(fstream);

			Step s = new Step();
			Iterator it = wf.steps.iterator();
			while(it.hasNext()) {
				s = (Step)it.next();
				out.write(s.getName() + ", ");
				Iterator ite = s.actions.iterator();
				while(ite.hasNext()) {	
					Action a = (Action)ite.next();
					out.write(a.getName() + ", " + wf.findStepNameByID(a.getTargetID())+ " ");
					out.write(",");
					if(a.conditions.size()!=0){
						out.write("\"");
						Iterator<String> itt = a.conditions.iterator();
						while(itt.hasNext()){
							out.write(itt.next()+ "\n");
						}
						out.write("\", ");
					}
					else {
						out.write(", ");
					}
					if(a.validators.size()!=0){
						out.write("\"");
						Iterator<String> itt = a.validators.iterator();
						while(itt.hasNext()){
							out.write(itt.next()+ "\n");
						}
						out.write("\", ");
					}
					else {
						out.write(", ");
					}
					if(a.postFunctions.size()!=0){
						out.write("\"");
						Iterator<String> itt = a.postFunctions.iterator();
						while(itt.hasNext()){
							out.write(itt.next()+ " ");
						}
						out.write("\" ");
					}
					else {
						out.write(", ");
					}
					out.write("\n ,");
				}
				out.write("\n");
			}

			out.close();

		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}