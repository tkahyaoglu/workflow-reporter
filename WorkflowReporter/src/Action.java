import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;



public class Action {
	int id;
	String name;
	int targetID;
	List<String> validators;
	List<String> conditions;
	List<String> postFunctions;
	List<String> unworthyPostFunctions = Arrays.asList("com.atlassian.jira.workflow.function.issue.IssueReindexFunction","com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction","com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction","com.atlassian.jira.workflow.function.misc.CreateCommentFunction");
	
	public Action(){
		
	}
	
	public Action(int id, String name,int targetID){
		this.id=id;
		this.name=name;
		this.targetID = targetID;
		this.validators = new ArrayList<String>();
		this.conditions= new ArrayList<String>();
		this.postFunctions= new ArrayList<String>();
	}
	
	public int getTargetID() {
		return targetID;
	}

	public void setTargetID(int targetID) {
		this.targetID = targetID;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isPostFunctionWorthy(String postFunctionClass){
		boolean flag =true;
		
		for(int i=0;i<unworthyPostFunctions.size();i++){
			if(postFunctionClass.equals(unworthyPostFunctions.get(i))){
				flag=false;
				break;
			}
		}
		return flag;
	}

}
