import java.util.ArrayList;
import java.util.List;

public class Step {
	int id;
	String name;
	List<Action> actions;
	
	public Step(){
		
	}
	
	public Step(int id, String name){
		this.id=id;
		this.name=name;
		this.actions = new ArrayList<Action>();
	}
	
	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
